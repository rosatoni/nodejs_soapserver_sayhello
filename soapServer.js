var http = require('http');
var soap = require('soap');

var helloService = {
  Hello_Service: {
    Hello_Port: {
      sayHello: function(args) {
        console.log("Args:", args);
        return {          
          greeting: "Hola " + args.firstName['$value'] + "!"
        };
      }
    }
  }
}
var xml = require('fs').readFileSync('serverSOAP.wsdl', 'utf8'),
      server = http.createServer(function(request,response) {
          response.end("404: Not Found: "+request.url)
      });
server.listen(8000);
var soapServer = soap.listen(server, '/', helloService, xml);
soapServer.log = function(type, data) {
    console.log("Connexió entrant: ",type);
};

/*
// Resum ---------------------------------------------------
// URL del WSDL: http://localhost:8000/?wsdl
// URL de l'endpoint http://localhost:8000/wsdl
 
// Exemple de Crida entrant SOAP correcte:
<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:examples:helloservice">
   <soapenv:Header/>
   <soapenv:Body>
      <urn:sayHello soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
         <firstName xsi:type="xsd:string">Toni</firstName>
      </urn:sayHello>
   </soapenv:Body>
</soapenv:Envelope>
*/
